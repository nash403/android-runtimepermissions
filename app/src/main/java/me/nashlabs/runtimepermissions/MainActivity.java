package me.nashlabs.runtimepermissions;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private View mainLayout;

    // Identifiants pour les requêtes de permission
    private static final int REQUEST_CONTACT = 0;

    // Les permissions READ_CONTACTS et WRITE_CONTACTS appartiennent au même groupe donc on peut les regrouper pour plus de cohérence
    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainLayout = findViewById(R.id.main_layout);

        // on instancie le fragment principal
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            PermissionFragment fragment = new PermissionFragment();
            transaction.replace(R.id.content_fragment, fragment);
            transaction.commit();
        }
    }

    public void showContact(View v) {
        // On vérifie si l'on possède les permissions requises
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            // si non, on les demande
            askPermissionContact();

        } else {
            // ici on a les permissions, on peut effectuer l'action voulue (afficher un contact)
            showContactFragment();
        }
    }

    public void showCamera(View v) {
        Toast.makeText(this,"Là c'est à vous de jouer !! ;)",Toast.LENGTH_SHORT).show();
    }

    private void askPermissionContact() {
        // On vérifie que ce n'est pas la première fois que l'on demande cette autorisation
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_CONTACTS)) {
            // si ce n'est pas la première fois, on peut montrer à un message à l'utilisateur pour expliquer pourquoi il faut qu'il accepte la demande de permission
            Snackbar.make(mainLayout, R.string.contact_rationale_perm,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // si l'utilisateur est convaincu et clique sur ok, lui redemander la permission
                            ActivityCompat
                                    .requestPermissions(MainActivity.this, PERMISSIONS_CONTACT,
                                            REQUEST_CONTACT);
                        }
                    })
                    .show();
        } else {
            // ici c'est la première fois qu'on demande la permission
            ActivityCompat.requestPermissions(this, PERMISSIONS_CONTACT, REQUEST_CONTACT);
        }
    }


    private void showContactFragment() {
        // chargement du fragement d'affichage du contact
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_fragment, new ContactFragment())
                .addToBackStack("contact")
                .commit();
    }

    private void showCameraFragment() {
        // chargement du fragement de prévisualisation de la caméra
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_fragment, new CameraFragment())
                .addToBackStack("camera")
                .commit();
    }

    public void onBack(View view) {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        // callback de retour du résultat d'une requête de permission
        if (requestCode == REQUEST_CONTACT) {
            // pour les contact
            if (checkForAll(grantResults)) { // comme on a demandé plusieurs permission en même temps, vérifier que toutes ont été acceptées
                // ici OK
                Snackbar.make(mainLayout, R.string.ok_perm_contact,
                        Snackbar.LENGTH_SHORT)
                        .show();
            } else {
                // ici KO
                Snackbar.make(mainLayout, R.string.ko_perm_contact,
                        Snackbar.LENGTH_SHORT)
                        .show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public static boolean checkForAll(int[] grantResults) {
        if (grantResults.length < 1) {
            return false;
        }

        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
}
