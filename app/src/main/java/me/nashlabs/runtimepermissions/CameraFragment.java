package me.nashlabs.runtimepermissions;

import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by nintunze on 09/11/2016.
 */

public class CameraFragment extends Fragment {
    private CameraPreview mPreview;
    private Camera mCamera;

    public CameraFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // On va chercher le layout pour prévisualiser
        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);

        // on instancie la caméra
        mCamera = getCameraInstance();

        // on instancie le prévisualiseur et on l'ajoute au layout
        mPreview = new CameraPreview(getActivity(), mCamera);
        FrameLayout preview = (FrameLayout) rootView.findViewById(R.id.preview);
        preview.addView(mPreview);

        return rootView;
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch (Exception e){}
        return c;
    }

    @Override
    public void onPause() {
        super.onPause();
        // On relâche les ressources de la caméra
        releaseCamera();
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }
}
