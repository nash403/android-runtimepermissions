package me.nashlabs.runtimepermissions;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PermissionFragment extends Fragment {


    public PermissionFragment(){}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // on charge simplement la vue
        return  inflater.inflate(R.layout.fragment_main, null);
    }

}
