package me.nashlabs.runtimepermissions;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by nintunze on 09/11/2016.
 */

public class ContactFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public ContactFragment() {}

    private TextView contactMessageText = null;

    private static final String[] PROJECTION = {ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY};

    private static final String ORDER = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " ASC";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact, container, false);

        contactMessageText = (TextView) rootView.findViewById(R.id.msg);

        Button button = (Button) rootView.findViewById(R.id.load);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayContact();
            }
        });
        return rootView;
    }

    private void displayContact() {
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(getActivity(), ContactsContract.Contacts.CONTENT_URI, PROJECTION,
                null, null, ORDER);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        contactMessageText.setText(R.string.contact_empty);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        if (cursor != null) {
            final int total = cursor.getCount();
            if (total > 0) {
                cursor.moveToFirst();
                String name = cursor
                        .getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contactMessageText.setText(
                        getResources().getString(R.string.contact_msg, total, name));
            } else {
                contactMessageText.setText(R.string.contact_empty);
            }
        }
    }
}
